const mix = require('laravel-mix');
require('laravel-mix-purgecss');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .styles([
        'public/front/assets/font-awesome/css/fontawesome-all.min.css',
        'public/front/assets/css/owl.carousel.min.css'
    ], 'public/css/all.css')
    .scripts([
        'public/assets/js/imagesloaded.pkgd.min.js',
        'public/front/assets/js/isInViewport.jquery.js',
        'public/front/assets/js/jquery.particleground.min.js',
        'public/front/assets/js/owl.carousel.min.js',
        'public/front/assets/js/scrolla.jquery.min.js',
        'public/front/assets/js/jquery.validate.min.js',
        'public/front/assets/js/jquery-validate.bootstrap-tooltip.min.js',
        'public/front/assets/js/jquery.wavify.js',
        'public/front/assets/js/custom.js',
        'public/front/assets/js/index.js',
        'public/front/assets/js/slick.js',
        'public/front/assets/js/slide.js',
        'public/front/assets/js/modclose.js',
        // 'public/js/dashboard.js'
    ], 'public/js/all.js')
    .purgeCss();
